import sys


class Calculadora():

    def __init__(self, op1, op2):
        self.op1 = op1
        self.op2 = op2

    def suma(self, op1, op2):
        return op1 + op2

    def resta(self, op1, op2):
        return op1 - op2


if __name__ == "__main__":

    try:
        op1 = int(sys.argv[1])
        op2 = int(sys.argv[3])
        calcula = Calculadora(op1, op2)
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    if sys.argv[2] == "suma":
        result = calcula.suma(op1, op2)

    elif sys.argv[2] == "resta":
        result = calcula.resta(op1, op2)

    else:
        sys.exit('Operación sólo puede ser sumar o restar.')

    print(result)
