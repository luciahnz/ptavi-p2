import sys
import calcoohija
from calcoohija import CalculadoraHija
import csv

if __name__ == "__main__":

    op1 = sys.argv[1]
    op2 = op1
    calhija = CalculadoraHija(op1, op2)

    with open(sys.argv[1], newline='') as File:
        reader = csv.reader(File)

        for row in reader:
            identificador = row[0]
            op3 = row[1]
            if identificador == "suma":
                for numero in row[2:]:
                    op3 = calhija.suma(int(op3), int(numero))

            elif identificador == "resta":
                for numero in row[2:]:
                    op3 = calhija.resta(int(op3), int(numero))

            elif identificador == "multiplica":
                for numero in row[2:]:
                    op3 = calhija.multiplicacion(int(op3), int(numero))

            elif identificador == "divide":
                for numero in row[2:]:
                    op3 = calhija.division(int(op3), int(numero))

            else:
                sys.exit("No funciona ")
            print(op3)
