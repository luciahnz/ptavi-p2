import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):

    def multiplicacion(self, op1, op2):
        return op1 * op2

    def division(self, op1, op2):
        return op1 / op2


if __name__ == "__main__":
    try:
        op1 = int(sys.argv[1])
        op2 = int(sys.argv[3])
        calcuhija = CalculadoraHija(op1, op2)

    except ValueError:
        sys.exit("Lo has escrito mal ¡Vuelve a intentarlo!")

    if sys.argv[2] == "suma":
        resultado = calcuhija.suma(op1, op2)

    elif sys.argv[2] == "resta":
        resultado = calcuhija.resta(op1, op2)

    elif sys.argv[2] == "multiplicacion":
        resultado = calcuhija.multiplicacion(op1, op2)

    elif sys.argv[2] == "division":
        try:
            resultado = calcuhija.division(op1, op2)
        except ZeroDivisionError:
            sys.exit("Esta division no se puede hacer")

    else:
        sys.exit('Sólo se puede sumar, restar, multiplicacion o division.')

    print(resultado)
